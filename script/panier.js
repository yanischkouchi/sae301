document.addEventListener('DOMContentLoaded', function () {
    // Récupérer l'élément .panier-items
    var panierItemsContainer = document.querySelector('.panier-items');

    // Appeler la fonction pour afficher les produits du panier
    displayCartItems();

    // Fonction pour afficher les produits du panier
    function displayCartItems() {
        // Récupérer le panier depuis le localStorage
        var cart = JSON.parse(localStorage.getItem('cart')) || [];

        // Réinitialiser le contenu de .panier-items
        panierItemsContainer.innerHTML = '';

        // Boucler à travers les produits dans le panier
        cart.forEach(function (item, index) {
            // Créer un élément pour chaque produit
            var itemElement = document.createElement('div');
            itemElement.classList.add('panier-item');

            // Afficher le nom et la quantité du produit
            var infoElement = document.createElement('div');
            infoElement.classList.add('panier-item-info');
            var nameElement = document.createElement('h4');
            nameElement.innerText = item.name;
            var quantityElement = document.createElement('span');
            quantityElement.innerText = 'Quantité: ' + item.quantity;
            infoElement.appendChild(nameElement);
            infoElement.appendChild(quantityElement);

            // Ajouter l'option "Supprimer" avec un bouton
            var deleteButton = document.createElement('img');
            deleteButton.src = './medias/delete.png'; // Remplacez ceci par le chemin réel de votre image "Supprimer"
            deleteButton.alt = 'Supprimer';
            deleteButton.addEventListener('click', function () {
                removeFromCart(index);
                displayCartItems(); // Mettre à jour l'affichage après la suppression
            });

            infoElement.appendChild(deleteButton);

            // Ajouter les informations à l'élément du produit
            itemElement.appendChild(infoElement);

            // Ajouter l'élément du produit à .panier-items
            panierItemsContainer.appendChild(itemElement);
        });

        // Appeler la fonction pour calculer le prix total et l'afficher
        displayTotalPrice();
    }

    // Fonction pour calculer et afficher le prix total du panier
    function displayTotalPrice() {
        var cart = JSON.parse(localStorage.getItem('cart')) || [];
        var totalPrice = 0;

        // Calculer le prix total en bouclant à travers les produits dans le panier
        cart.forEach(function (item) {
            totalPrice += item.totalPrice;
        });

        // Récupérer l'élément .total-price
        var totalPriceElement = document.querySelector('.total-price');

        // Afficher le prix total dans l'élément .total-price
        totalPriceElement.innerText = 'Prix total: ' + totalPrice + '€';
    }

    // Fonction pour supprimer un produit du panier
    function removeFromCart(index) {
        var cart = JSON.parse(localStorage.getItem('cart')) || [];

        // Supprimer l'élément du tableau du panier
        cart.splice(index, 1);

        // Mettre à jour le panier dans le localStorage
        localStorage.setItem('cart', JSON.stringify(cart));
    }
});

// Fonction pour la redirection de page
function redirectToPage(page) {
    window.location.href = page;
}