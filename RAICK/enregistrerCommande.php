<?php
$servername = "localhost";
$username = "username";
$password = "password";
$dbname = "gestion_de_commande";

// Créer une connexion
$conn = new mysqli($servername, $username, $password, $dbname);

// Vérifier la connexion
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// Supposons que l'ID de la commande est stocké dans $_SESSION['order_id']
$order_id = $_SESSION['order_id'];

$sql = "SELECT Clients.Nom, Clients.Email, Plats.Nom, Plats.Prix FROM Commandes 
JOIN Clients ON Commandes.ClientID = Clients.ID 
JOIN Plats ON Commandes.PlatID = Plats.ID 
WHERE Commandes.ID = $order_id";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // Afficher les données de chaque ligne
  while($row = $result->fetch_assoc()) {
    echo "Nom: " . $row["Nom"]. " - Email: " . $row["Email"]. " - Plat: " . $row["Nom"]. " - Prix: " . $row["Prix"]. "<br>";
  }
} else {
  echo "0 results";
}
$conn->close();
?>
