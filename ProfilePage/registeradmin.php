<?php

session_start();

include 'config.php';
require_once('classes/Class.Admin.php');
require_once('classes/Class.Validate.php');

// Création d'une instance de la classe Admin
$admin = new Admin($conn);

// Création d'une instance de la classe Validate
$validate = new Validate($conn);

// Si l'administrateur est déjà connecté, redirection vers la page d'accueil
if ($admin->isLoggedIn()) 
{
    $admin->redirect('home.php');
}

// Initialisation du tableau d'erreurs
$errors = array();

// Lorsque le bouton d'inscription est pressé, procéder à l'inscription
if (isset($_POST['register'])) 
{
    // Récupération des données du formulaire
    $uname = $_POST['adminname'];
    $email = $_POST['email'];
    $pass = $_POST['password'];
    $pass2 = $_POST['cpassword'];
    
    // Validation du nom d'administrateur
    if ($validate->adminnameValidate($uname) != null) 
    {
        // Ajout de l'erreur au tableau des erreurs s'il y en a une
        $errors[] = $validate->adminnameValidate($uname);
    }

    // Validation de l'adresse e-mail de l'administrateur
    if ($validate->emailValidate($email) != null) 
    {
        // Ajout de l'erreur au tableau des erreurs s'il y en a une
        $errors[] = $validate->emailValidate($email);
    }

    // Validation du mot de passe de l'administrateur
    if ($validate->passwordValidate($pass, $pass2) != null) 
    {
        // Ajout de l'erreur au tableau des erreurs s'il y en a une
        $errors[] = $validate->passwordValidate($pass, $pass2);
    }
    
    // Si aucune erreur de validation n'est présente, procéder à l'inscription
    if (empty($errors)) 
    {
        // Tentative d'inscription avec les données fournies
        if($admin->register($uname, $email, $pass) === true) 
        {
            // Redirection vers la page de connexion avec un message de succès
            $admin->redirect('registeradmin.php?joined');
        }
        else 
        {
            // Affichage d'un message d'erreur en cas d'échec de l'inscription
            echo 'Erreur lors de l\'inscription, veuillez réessayer.';
        }
    }
    else
    {
        // Affichage de toutes les erreurs de validation
        foreach ($errors as $error) 
        {
            printf ($error . "<br/>");
        }
    }   
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inscription</title>
        <link rel="stylesheet" href="css/style.css">
    </head>

    <body>
        <div class="form-container">

            <?php
                if (isset($_GET['joined'])) {
            ?>
            <p>Inscription réussie, vous pouvez maintenant vous <a href="homeadmin.php">connecter</a>.</p>
            <?php
            } else {
            ?>
            <form method="post" action="registeradmin.php" name="registerform">

                <!-- Img cliquable redirigeant vers register.php -->
                <a href="register.php">
                   <img src="images/User.png" alt="Logo" width="25" height="25">
                </a>

                <h3>Inscription</h3>
                <input type="text" name="adminname" placeholder="Entrez votre nom d'admin..." class="box" required />
                <input type="email" name="email" placeholder="Entrez votre adresse mail..." class="box" required />
                <input type="password" name="password" auto_complete="off" placeholder="Entrez votre mot de passe..." class="box" required />
                <input type="password" name="cpassword" auto_complete="off" placeholder="Confirmez votre mot de passe..." class="box" required />
                <input type="submit" name="register" value="S'Inscrire" class="btn"/>
                <p>Vous avez déjà un compte ? <a href="loginadmin.php">Se Connecter</a></p>
                <a href="../accueil.html">
                   <img src="../medias/home.png" alt="Accueil" width="50" height="50" style="margin-right: 50px;">
                </a>
                <a href="../panier.html">
                   <img src="../medias/panier.png" alt="Panier" width="50" height="50" style="margin-top: 50px;">
                </a>
                <a href="home.php">
                   <img src="../medias/compte.png" alt="Profil" width="50" height="50" style="margin-left: 50px;">
                </a>
            </form>
            <?php
            }
            ?> 

        </div>
    </body>

</html>