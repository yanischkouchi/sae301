<?php
// Connexion à la bdd MySQL

$conn = mysqli_connect('localhost','root','','test') or die('connection failed');

/* Table MySQL :
CREATE TABLE users (
  id int(100) NOT NULL,
  name varchar(100) NOT NULL,
  email varchar(100) NOT NULL,
  password varchar(100) NOT NULL,
  dob date DEFAULT NULL,
  image varchar(100) NOT NULL
);

ALTER TABLE users
  ADD PRIMARY KEY (id);

ALTER TABLE users
  MODIFY id int(100) NOT NULL AUTO_INCREMENT;
COMMIT;

------------------------------------------------

CREATE TABLE IF NOT EXISTS `admins` (
  `adminId` int(11) NOT NULL AUTO_INCREMENT,
  `adminName` varchar(64) NOT NULL,
  `adminEmail` varchar(64) NOT NULL,
  `adminPasswordHash` varchar(255) NOT NULL,
  PRIMARY KEY (`adminId`),
  UNIQUE KEY `adminName` (`adminName`),
  UNIQUE KEY `adminEmail` (`adminEmail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
*/
?>