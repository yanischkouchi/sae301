<?php

session_start();

include 'config.php';
require_once('classes/Class.Admin.php');

// Instanciation de la classe Admin avec la connexion à la base de données
$admin = new Admin($conn);

// Si l'admin n'est pas connecté => redirection vers la page d'accueil
if (!$admin->isLoggedIn()) 
{
    $admin->redirect('homeadmin.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profil Admin</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <p>Bonjour, <?php echo $_SESSION['adminName']; ?>. Vous vous êtes connecté avec succés !</p>
    <p><a href="logoutadmin.php?logout">Déconnexion</a></p>
</body>
</html>