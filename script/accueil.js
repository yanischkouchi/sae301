// CAROUSSEL 1 //

var $images = $("#carrousel1>img");
var $textElements = $("#carrousel1 .img-info");

// Image actuellement affichée
var currentIndex = 0;

// Mettre à jour le texte
function updateText(index) {
    $textElements.hide(); // masquer tous les textes
    $textElements.eq(index).show(); // afficher le texte correspondant
}

// Mettre à jour l'image (et le texte qui correspond)
function changeImage(index) {
    $images.hide(); // masquer toutes les images
    $images.eq(index).show(); // afficher l'image correspondante
    updateText(index); // mettre à jour le texte correspondant
}

// Définir ce qu'est l'image suivante
function nextImage() {
  currentIndex = (currentIndex + 1) % $images.length;
  changeImage(currentIndex);
}

// Changer d'image lorsqu'on click sur les flèches
$("#carrousel1>p:first-of-type").on("click", function () {
  currentIndex = (currentIndex - 1 + $images.length) % $images.length;
  changeImage(currentIndex);
});

$("#carrousel1>p:nth-of-type(2)").on("click", function () {
  currentIndex = (currentIndex + 1) % $images.length;
  changeImage(currentIndex);
});

// Commencer avec la 1e img et le 1e texte
updateText(currentIndex);
changeImage(currentIndex);

// CAROUSSEL 2 //

var $images2 = $(".article-promos");
var $textElements2 = $(".article-promos .img-info2");

// Image actuellement affichée
var currentIndex2 = 0;

// Mettre à jour le texte
function updateText2(index) {
    $textElements2.hide(); // masquer tous les textes

    // Afficher les deux textes correspondants aux deux images affichées
    $textElements2.eq(index).show();
    $textElements2.eq((index + 1) % $images2.length).show();
}

// Mettre à jour les images (et le texte qui correspond)
function changeImages(index) {
    $images2.hide(); // masquer toutes les images

    // Afficher les deux images correspondantes
    $images2.eq(index).show();
    $images2.eq((index + 1) % $images2.length).show();

    updateText2(index); // mettre à jour le texte correspondant
}

// Définir ce qu'est l'image suivante
function nextImages() {
    currentIndex2 = (currentIndex2 + 2) % $images2.length;
    changeImages(currentIndex2);
}

// Changer d'image lorsqu'on click sur les flèches
$("#carrousel2>p:first-of-type").on("click", function () {
    currentIndex2 = (currentIndex2 - 2 + $images2.length) % $images2.length;
    changeImages(currentIndex2);
});

$("#carrousel2>p:nth-of-type(2)").on("click", function () {
    currentIndex2 = (currentIndex2 + 2) % $images2.length;
    changeImages(currentIndex2);
});

// Commencer avec les 2 premières images et les textes correspondants
updateText2(currentIndex2);
changeImages(currentIndex2);