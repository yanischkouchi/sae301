<?php
// Pour accéder à la bdd
include 'config.php';
// Démarrage de la session pour récupérer l'ID de l'utilisateur connecté
session_start();
$user_id = $_SESSION['user_id'];

// Si l'ID de l'utilisateur n'est pas défini => redirection vers le login
if(!isset($user_id)){
   header('location:login.php');
};

// Déconnexion de l'utilisateur et redirection vers la page de connexion si demandé (si logout est dans l'url)
if(isset($_GET['logout'])){
   unset($user_id); // Suppression de la variable d'ID utilisateur
   session_destroy(); // Destruction de la session
   header('location:login.php'); // Redirection vers la page de connexion
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Profil</title>
   <link rel="stylesheet" href="css/style.css">

</head>
<body>
   
<div class="container">

   <div class="profile">
      <?php
         // Récupération des données de l'utilisateur
         $select = mysqli_query($conn, "SELECT * FROM `users` WHERE id = '$user_id'") or die('query failed');
         if(mysqli_num_rows($select) > 0){
            $fetch = mysqli_fetch_assoc($select);
         }

         // Vérification si l'utilisateur a une image de profil
         if($fetch['image'] == ''){
            echo '<img src="images/ParDéfaut.png">';
         }else{
            echo '<img src="uploaded_img/'.$fetch['image'].'">';
         }
      ?>
      <!-- Affichage du nom de l'utilisateur -->
      <h3><?php echo $fetch['name']; ?></h3>
      
      <!-- Liens pour modifier le profil et se déconnecter -->
      <a href="update_profile.php" class="btn">Modifier le Profil</a>
      <a href="home.php?logout=<?php echo $user_id; ?>" class="delete-btn">Déconnexion</a>
      
      <!-- Liens pour nouvelle connexion ou inscription -->
      <p>Nouvelle <a href="login.php">Connexion</a> ou <a href="register.php">Inscription</a></p>
      <a href="../accueil.html">
         <img src="../medias/home.png" alt="Accueil" width="50" height="50" style="margin-right: 50px;">
      </a>
      <a href="../panier.html">
         <img src="../medias/panier.png" alt="Panier" width="50" height="50" style="margin-top: 50px;">
      </a>
      <a href="home.php">
         <img src="../medias/compte.png" alt="Profil" width="50" height="50" style="margin-left: 50px;">
      </a>
   </div>

</div>

</body>
</html>