/* HEADER */
function createHeader() {
    const header = document.querySelector('header');

    // Crée la div pour envelopper la barre de recherche
    const searchBarContainer = document.createElement('div');
    searchBarContainer.className = 'search-bar-container';

    const homeLogo = document.createElement('a');
    homeLogo.href = './accueil.html';
    homeLogo.className = 'logo';
    homeLogo.innerHTML = '<img src="./medias/logo.jpg" alt="logo">';

    const searchForm = document.createElement('form');
    searchForm.className = 'search-form';
    searchForm.action = '#';
    searchForm.method = 'get';

    const searchInput = document.createElement('input');
    searchInput.type = 'text';
    searchInput.className = 'search-input';
    searchInput.name = 'search';
    searchInput.placeholder = 'Rechercher...';

    const searchButton = document.createElement('button');
    searchButton.type = 'submit';
    searchButton.textContent = 'Rechercher';
    searchButton.className = 'search'

    searchForm.appendChild(searchInput);
    searchForm.appendChild(searchButton);

    // Ajoute le logo "home" à gauche de la barre de recherche
    searchBarContainer.appendChild(homeLogo);

    // Ajoute la barre de recherche au centre de la div
    searchBarContainer.appendChild(searchForm);

    // Ajoute la div de la barre de recherche à l'en-tête
    header.appendChild(searchBarContainer);

    // Crée le burger menu
    const burgerMenu = document.createElement('div');
    burgerMenu.className = 'burger-menu';

    const menuIcon = document.createElement('a');
    menuIcon.className = 'menu-icon';
    menuIcon.innerHTML = '<img src="./medias/menu.png" alt="logo">';

    const menuOptions = document.createElement('div');
    menuOptions.className = 'menu-options';

    const menuItems = ['Entrées', 'Plats', 'Desserts', 'Boissons', 'Formules'];

    menuItems.forEach(itemText => {
        const menuItem = document.createElement('a');
        menuItem.href = './plats.html';  // Ajoutez les liens appropriés
        menuItem.textContent = itemText;
        menuOptions.appendChild(menuItem);
    });

    burgerMenu.appendChild(menuIcon);
    burgerMenu.appendChild(menuOptions);

    // Ajoute le burger menu à l'en-tête
    header.appendChild(burgerMenu);

    // Ajoute l'événement de clic pour ouvrir/fermer le menu
    burgerMenu.addEventListener('click', function() {
        menuOptions.style.display = menuOptions.style.display === 'none' ? 'flex' : 'none';
    });
}

document.addEventListener('DOMContentLoaded', function() {
    createHeader();

    const searchInput = document.querySelector('.search-input');

    searchInput.addEventListener('input', function() {
        const query = searchInput.value.trim();
        filterPlatsBySearch(query);
    });
});

//recherche avancée
function filterPlatsBySearch(query) {
    const allSections = document.querySelectorAll('section'); // Sélectionnez toutes les sections

    allSections.forEach(section => {
        const articles = section.querySelectorAll('.article-' + section.classList[0]); // Ajoutez la classe de la section au sélecteur
        const h2Title = section.querySelector('h2');

        let anyMatch = false; // Variable pour vérifier s'il y a une correspondance avec la recherche

        // Exclure la section des plats populaires de l'affichage lors de la recherche
        if (section.classList.contains('populaires') && query.trim() !== '') {
            section.style.display = 'none';
            return; // Sortir de la boucle pour la section des plats populaires
        }

        // Recherche dans le titre h2
        if (h2Title) {
            const shouldDisplayTitle = h2Title.textContent.toLowerCase().includes(query.toLowerCase());
            h2Title.style.display = shouldDisplayTitle ? 'block' : 'none';
            if (shouldDisplayTitle) {
                anyMatch = true;
            }
        }

        // Recherche dans les articles
        articles.forEach(article => {
            const platName = article.querySelector('.plat-' + section.classList[0]); // Ajoutez la classe de la section au sélecteur
            const shouldDisplay = platName.textContent.toLowerCase().includes(query.toLowerCase());

            article.style.display = shouldDisplay ? 'block' : 'none';

            if (shouldDisplay) {
                anyMatch = true;
            }
        });

        // Gérer la visibilité du titre et de la section en fonction des résultats de recherche
        if (h2Title || anyMatch) {
            section.style.display = 'block';
        } else {
            section.style.display = 'none';
        }
    });
}

// Fonction pour la redirection vers une autre page
function redirectToPage(url) {
    window.location.href = url;
}


/* FOOTER */
function createFooter() {
    // Créer le conteneur du footer
const footerContainer = document.createElement('div');
footerContainer.className = 'footer-container';

// Lien pour home
const homeLink = document.createElement('a');
homeLink.href = './accueil.html';
homeLink.className = 'home-logo';
homeLink.innerHTML = '<img src="./medias/home.png" alt="Home">';

// Lien pour le panier
const cartLink = document.createElement('a');
cartLink.href = './panier.html';
cartLink.className = 'panier-logo';
cartLink.innerHTML = '<img src="./medias/panier.png" alt="Panier">';

// Lien pour la gestion du compte
const accountLink = document.createElement('a');
accountLink.href = 'ProfilePage/home.php';
accountLink.className = 'compte-logo';
accountLink.innerHTML = '<img src="./medias/compte.png" alt="Gestion de compte">';

// Ajouter les liens au footer
footerContainer.appendChild(homeLink);
footerContainer.appendChild(cartLink);
footerContainer.appendChild(accountLink);

// Ajouter le footerContainer à la page
document.querySelector('footer').appendChild(footerContainer);
}


document.addEventListener('DOMContentLoaded', function() {
    createFooter();
});