// Définition de la classe de base Carousel
class Carousel {
    // Le constructeur prend en paramètre des sélecteurs pour le carrousel et les plats
    constructor(selector, platSelector) {
        // Sélection des éléments du DOM pour le carrousel et les plats
        this.$carousel = $(selector);
        this.$plats = $(platSelector);
        // Index actuel dans le carrousel
        this.currentIndex = 0;
    }

    // Méthode pour mettre à jour le texte des plats en fonction de l'index
    updateText(index) {
        this.$plats.hide(); // Cacher tous les plats
        this.$plats.eq(index).show(); // Afficher le plat à l'index actuel
        this.$plats.eq((index + 1) % this.$plats.length).show(); // Afficher le plat suivant (en boucle)
    }

    // Méthode pour changer les images du carrousel en fonction de l'index
    changeImages(index) {
        this.$carousel.hide(); // Cacher toutes les images du carrousel
        this.$carousel.eq(index).show(); // Afficher l'image du carrousel à l'index actuel
        this.$carousel.eq((index + 1) % this.$carousel.length).show(); // Afficher l'image suivante (en boucle)
        this.updateText(index); // Mettre à jour le texte des plats
    }

    // Méthode pour passer aux images suivantes
    nextImages() {
        this.currentIndex = (this.currentIndex + 2) % this.$carousel.length; // Incrémenter l'index
        this.changeImages(this.currentIndex); // Appliquer le changement d'images
    }
}

// Classes spécifiques pour différents types de carrousels, héritant de la classe Carousel
class PopulairesCarousel extends Carousel {
    constructor() {
        super('.article-populaires', '.plat-populaires'); // Appel du constructeur de la classe parent avec les sélecteurs spécifiques
    }
}

class ChaudsCarousel extends Carousel {
    constructor() {
        super('.article-chauds', '.plat-chauds');
    }
}

class PizzasCarousel extends Carousel {
    constructor() {
        super('.article-pizzas', '.plat-pizzas');
    }
}

class VegCarousel extends Carousel {
    constructor() {
        super('.article-veg', '.plat-veg');
    }
}

class LocauxCarousel extends Carousel {
    constructor() {
        super('.article-locaux', '.plat-locaux');
    }
}

class SoupesCarousel extends Carousel {
    constructor() {
        super('.article-soupes', '.plat-soupes');
    }
}

// Instanciation des carrousels spécifiques
const populairesCarousel = new PopulairesCarousel();
const chaudsCarousel = new ChaudsCarousel();
const pizzasCarousel = new PizzasCarousel();
const vegCarousel = new VegCarousel();
const locauxCarousel = new LocauxCarousel();
const soupesCarousel = new SoupesCarousel();

// Initialisation des gestionnaires d'événements pour le carrousel Populaires
$('.carrouselpop>p:first-of-type').on('click', function () {
    populairesCarousel.currentIndex = (populairesCarousel.currentIndex - 2 + populairesCarousel.$carousel.length) % populairesCarousel.$carousel.length;
    populairesCarousel.changeImages(populairesCarousel.currentIndex);
});

$('.carrouselpop>p:nth-of-type(2)').on('click', function () {
    populairesCarousel.nextImages();
});

// Initialisation des gestionnaires d'événements pour le carrousel Chauds
$('.carrouselchauds>p:first-of-type').on('click', function () {
    chaudsCarousel.currentIndex = (chaudsCarousel.currentIndex - 2 + chaudsCarousel.$carousel.length) % chaudsCarousel.$carousel.length;
    chaudsCarousel.changeImages(chaudsCarousel.currentIndex);
});

$('.carrouselchauds>p:nth-of-type(2)').on('click', function () {
    chaudsCarousel.nextImages();
});

// Initialisation des gestionnaires d'événements pour le carrousel Pizzas
$('.carrouselpizzas>p:first-of-type').on('click', function () {
    pizzasCarousel.currentIndex = (pizzasCarousel.currentIndex - 2 + pizzasCarousel.$carousel.length) % pizzasCarousel.$carousel.length;
    pizzasCarousel.changeImages(pizzasCarousel.currentIndex);
});

$('.carrouselpizzas>p:nth-of-type(2)').on('click', function () {
    pizzasCarousel.nextImages();
});

// Initialisation des gestionnaires d'événements pour le carrousel Veg
$('.carrouselveg>p:first-of-type').on('click', function () {
    vegCarousel.currentIndex = (vegCarousel.currentIndex - 2 + vegCarousel.$carousel.length) % vegCarousel.$carousel.length;
    vegCarousel.changeImages(vegCarousel.currentIndex);
});

$('.carrouselveg>p:nth-of-type(2)').on('click', function () {
    vegCarousel.nextImages();
});

// Initialisation des gestionnaires d'événements pour le carrousel Locaux
$('.carrousellocaux>p:first-of-type').on('click', function () {
    locauxCarousel.currentIndex = (locauxCarousel.currentIndex - 2 + locauxCarousel.$carousel.length) % locauxCarousel.$carousel.length;
    locauxCarousel.changeImages(locauxCarousel.currentIndex);
});

$('.carrousellocaux>p:nth-of-type(2)').on('click', function () {
    locauxCarousel.nextImages();
});

// Initialisation des gestionnaires d'événements pour le carrousel Soupes
$('.carrouselsoupes>p:first-of-type').on('click', function () {
    soupesCarousel.currentIndex = (soupesCarousel.currentIndex - 2 + soupesCarousel.$carousel.length) % soupesCarousel.$carousel.length;
    soupesCarousel.changeImages(soupesCarousel.currentIndex);
});

$('.carrouselsoupes>p:nth-of-type(2)').on('click', function () {
    soupesCarousel.nextImages();
});

// Au chargement du document, mettre à jour et changer les images pour tous les carrousels
$(document).ready(function () {
    populairesCarousel.updateText(populairesCarousel.currentIndex);
    populairesCarousel.changeImages(populairesCarousel.currentIndex);

    chaudsCarousel.updateText(chaudsCarousel.currentIndex);
    chaudsCarousel.changeImages(chaudsCarousel.currentIndex);

    pizzasCarousel.updateText(pizzasCarousel.currentIndex);
    pizzasCarousel.changeImages(pizzasCarousel.currentIndex);

    vegCarousel.updateText(vegCarousel.currentIndex);
    vegCarousel.changeImages(vegCarousel.currentIndex);

    locauxCarousel.updateText(locauxCarousel.currentIndex);
    locauxCarousel.changeImages(locauxCarousel.currentIndex);

    soupesCarousel.updateText(soupesCarousel.currentIndex);
    soupesCarousel.changeImages(soupesCarousel.currentIndex);
});

// Script pour la redirection vers la page produit
document.addEventListener('DOMContentLoaded', () => {
    const articles = document.querySelectorAll('div>article');

    articles.forEach(article => {
        article.addEventListener('click', () => {
            const nom = article.getAttribute('data-nom');
            const image = article.getAttribute('data-image');
            const prix = article.getAttribute('data-prix');
            const ingredients = article.getAttribute('data-ingredients');

            // Construire le chemin vers la page produit avec les données spécifiques
            const pageProduit = `./produits.html?nom=${encodeURIComponent(nom)}&image=${encodeURIComponent(image)}&ingredients=${encodeURIComponent(ingredients)}&prix=${encodeURIComponent(prix)}`;

            redirectToPage(pageProduit);
        });
    });
});

// Fonction pour la redirection de page
function redirectToPage(page) {
    window.location.href = page;
}