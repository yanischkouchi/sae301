<?php
include 'config.php';
session_start();

// Vérification si le formulaire a été soumis
if(isset($_POST['submit'])){

   // Récupération et échappement de l'adresse e-mail depuis le formulaire
   $email = mysqli_real_escape_string($conn, $_POST['email']);

   // Récupération du mot de passe depuis le formulaire et application de la fonction de hachage (MD5)
   $pass = mysqli_real_escape_string($conn, md5($_POST['password']));

   // Exécution de la requête SQL pour sélectionner l'utilisateur avec l'email et le mot de passe fournis
   $select = mysqli_query($conn, "SELECT * FROM `users` WHERE email = '$email' AND password = '$pass'") or die('query failed');

   // Vérification du nombre de lignes retournées par la requête
   if(mysqli_num_rows($select) > 0){
      // Si au moins une ligne est retournée, récupération des informations de l'utilisateur
      $row = mysqli_fetch_assoc($select);
      
      // Stockage de l'identifiant de l'utilisateur dans la session
      $_SESSION['user_id'] = $row['id'];

      // Redirection vers la page d'accueil
      header('location:home.php');
   }else{
      // Si la requête ne retourne aucune ligne, ajout d'un message d'erreur
      $message[] = 'Adresse Mail ou Mdp érroné... !';
   }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Connexion</title>
   <link rel="stylesheet" href="css/style.css">
</head>
<body>
   
<div class="form-container">

   <form action="" method="post">

   <!-- Img cliquable redirigeant vers loginadmin.php -->
   <a href="loginadmin.php">
      <img src="images/Admin.png" alt="Logo" width="25" height="25">
   </a>

      <h3>Connexion</h3>
      <?php
      // Vérification si des messages d'erreur existent
      if(isset($message)){
         // Boucle pour afficher chaque message d'erreur dans une div avec la classe "message"
         foreach($message as $message){
            echo '<div class="message">'.$message.'</div>';
         }
      }
      ?> 
      <input type="email" name="email" placeholder="Entrez votre adresse mail..." class="box" required>
      <input type="password" name="password" placeholder="Entrez votre mot de passe..." class="box" required>
      <input type="submit" name="submit" value="Connexion" class="btn">
      <p>Vous n'avez pas de compte ? <a href="register.php">S'Inscrire maintenant</a></p>
      <a href="../accueil.html">
         <img src="../medias/home.png" alt="Accueil" width="50" height="50" style="margin-right: 50px;">
      </a>
      <a href="../panier.html">
         <img src="../medias/panier.png" alt="Panier" width="50" height="50" style="margin-top: 50px;">
      </a>
      <a href="home.php">
         <img src="../medias/compte.png" alt="Profil" width="50" height="50" style="margin-left: 50px;">
      </a>
   </form>

</div>

</body>
</html>