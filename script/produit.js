document.addEventListener('DOMContentLoaded', function () {
    // Récupérer les éléments du DOM
    var addButton = document.getElementById('add');
    var quantitySelect = document.getElementById('nbr');

    // Ajouter un écouteur d'événement au bouton "Ajouter au panier"
    addButton.addEventListener('click', function () {
        // Récupérer les informations du produit actuel
        var productName = document.querySelector('h2').innerText;
        var productPrice = parseFloat(document.querySelector('#prixProduit').innerText.replace('€', ''));
        var selectedQuantity = parseInt(quantitySelect.value);

        // Calculer le prix total en fonction de la quantité sélectionnée
        var totalPrice = productPrice * selectedQuantity;

        // Créer un objet représentant le produit
        var product = {
            name: productName,
            price: productPrice,
            quantity: selectedQuantity,
            totalPrice: totalPrice
        };

        // Stocker le produit dans le panier 
        addToCart(product);

        // Afficher un message
        alert('Le produit a été ajouté au panier.');
    });

    function addToCart(product) {
        // Vérifier si le panier existe dans le localStorage
        var cart = JSON.parse(localStorage.getItem('cart')) || [];

        // Vérifier si le produit est déjà dans le panier
        var existingProductIndex = cart.findIndex(function (item) {
            return item.name === product.name;
        });

        if (existingProductIndex !== -1) {
            // Si le produit existe, mettre à jour la quantité
            cart[existingProductIndex].quantity += product.quantity;
        } else {
            // Sinon, ajouter le produit au panier
            cart.push(product);
        }

        // Mettre à jour le panier dans le localStorage
        localStorage.setItem('cart', JSON.stringify(cart));
    }
});

// Fonction pour la redirection de page
function redirectToPage(page) {
    window.location.href = page;
}

// Récupération des paramètres de l'URL
const params = new URLSearchParams(window.location.search);
const nomProduit = params.get('nom');
const imageProduit = params.get('image');
const ingredientsProduit = params.get('ingredients');
const prixProduit = params.get('prix');

// Affichage des données du produit
document.getElementById('nomProduit').innerText = nomProduit;
document.getElementById('imageProduit').src = decodeURIComponent(imageProduit);
document.getElementById('ingredientsProduit').innerText = decodeURIComponent(ingredientsProduit);
document.getElementById('prixProduit').innerText = prixProduit;

/* SIMILAIRES */
var $pop = $(".article-similaires");
var $PlatsPop = $(".plat-similaires");

// Image actuellement affichée
var currentIndex2 = 0;

// Mettre à jour le texte
function updateText2(index) {
    $PlatsPop.hide(); // masquer tous les textes

    // Afficher les deux textes correspondants aux deux images affichées
    $PlatsPop.eq(index).show();
    $PlatsPop.eq((index + 1) % $pop.length).show();
}

// Mettre à jour les images (et le texte qui correspond)
function changeImages(index) {
    $pop.hide(); // masquer toutes les images

    // Afficher les deux images correspondantes
    $pop.eq(index).show();
    $pop.eq((index + 1) % $pop.length).show();

    updateText2(index); // mettre à jour le texte correspondant
}

// Définir ce qu'est l'image suivante
function nextImages() {
    currentIndex2 = (currentIndex2 + 2) % $pop.length;
    changeImages(currentIndex2);
}

// Changer d'image 
$(".carrouselsim>p:first-of-type").on("click", function () {
    currentIndex2 = (currentIndex2 - 2 + $pop.length) % $pop.length;
    changeImages(currentIndex2);
});

$(".carrouselsim>p:nth-of-type(2)").on("click", function () {
    currentIndex2 = (currentIndex2 + 2) % $pop.length;
    changeImages(currentIndex2);
});

// Commencer avec les 2 premi ères images et les textes correspondants
updateText2(currentIndex2);
changeImages(currentIndex2);