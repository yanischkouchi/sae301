document.getElementById('cb-button').addEventListener('click', function() {
    document.getElementById('checkbox-container').classList.remove('hidden');
});

document.getElementById('delivery-checkbox').addEventListener('change', function() {
    if (this.checked) {
        document.getElementById('message').classList.remove('hidden');
    } else {
        document.getElementById('message').classList.add('hidden');
    }
});

// partie commande 
// s'execute une fois le DOM complétement chargé 
document.addEventListener('DOMContentLoaded', function () {

    // Ajouter le gestionnaire d'événements pour le bouton "PAYER"
    document.getElementById('payer').addEventListener('click', function () {
        getCommandeDetails();
    });


    function getCardDetails() {
        var cardholderName = document.getElementById('cardholder-name').value;
        var cardNumber = document.getElementById('card-number').value;
        var expiryDate = document.getElementById('expiry-date').value;
        var cvv = document.getElementById('cvv').value;

        return {
            cardholderName: cardholderName,
            cardNumber: cardNumber,
            expiryDate: expiryDate,
            cvv: cvv
        };
    }

    function getCommandeDetails() {
        // Récupérer les détails de la carte
        var cardDetails = getCardDetails();

        // Récupérer le panier depuis le localStorage
        var cart = JSON.parse(localStorage.getItem('cart')) || [];

        // Réinitialiser le contenu de #order-details
        var orderDetailsContainer = document.getElementById('order-details');
        orderDetailsContainer.innerHTML = '';

        // Boucler à travers les produits dans le panier
        cart.forEach(function (item) {
            // Créer un élément pour chaque produit
            var itemElement = document.createElement('div');
            itemElement.classList.add('order-item');

            // Afficher le nom, la quantité, le prix unitaire et le prix total du produit
            var nameElement = document.createElement('h4');
            nameElement.innerText = item.name;

            var quantityElement = document.createElement('span');
            quantityElement.innerText = 'Quantité: ' + item.quantity;

            var unitPriceElement = document.createElement('span');
            unitPriceElement.innerText = 'Prix unitaire: ' + item.price + '€';

            var totalPriceElement = document.createElement('span');
            totalPriceElement.innerText = 'Prix total: ' + item.totalPrice + '€';

            // Ajouter les informations à l'élément du produit
            itemElement.appendChild(nameElement);
            itemElement.appendChild(quantityElement);
            itemElement.appendChild(unitPriceElement);
            itemElement.appendChild(totalPriceElement);

            // Ajouter l'élément du produit à #order-details
            orderDetailsContainer.appendChild(itemElement);
        });

        // Afficher les détails de la carte
        var cardDetailsElement = document.createElement('div');
        cardDetailsElement.classList.add('card-details');
        cardDetailsElement.innerHTML = '<h2>DETAILS DE LA CARTE</h2>';
        
        for (var key in cardDetails) {
            var detailElement = document.createElement('span');
            detailElement.innerText = key + ': ' + cardDetails[key];
            cardDetailsElement.appendChild(detailElement);
        }

        // Ajouter les détails de la carte à #order-details
        orderDetailsContainer.appendChild(cardDetailsElement);

        // Appeler la fonction pour calculer le prix total et l'afficher
        displayTotalPrice();
    }

});