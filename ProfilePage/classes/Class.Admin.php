<?php

Class Admin {
    
    private $db;
    
    // Constructeur de la classe, initialise la connexion à la base de données
    public function __construct ($conn) {  
    
        $this->db = $conn;  
        
    }
    
    // Enregistre un nouvel admin dans la base de données
    public function register($uname, $email, $pass) {  
    
        // Hashage du mot de passe
        $passHash = password_hash($pass, PASSWORD_DEFAULT);
        
        // Préparation de la requête SQL avec des paramètres pour éviter les injections SQL
        $stmt = $this->db->prepare("INSERT INTO admins (adminName, adminEmail, adminPasswordHash) VALUES (?,?,?)");
        $stmt->bind_param("sss", $uname, $email, $passHash);
        
        // Exécution de la requête SQL
        if($stmt->execute()) {
            return true; // Retourne true en cas de succès
        } else {
            return $stmt->error; // Retourne l'erreur en cas d'échec
        }
          
    }
    
    // Vérifie l'existence d'un admin dans la base de données lors de la connexion
    public function login($admin, $pass) {
        
        // Requête SQL pour récupérer les informations en fonction du nom d'admin ou de l'adresse mail
        $stmt = $this->db->prepare("SELECT adminName, adminEmail, adminPasswordHash FROM admins WHERE adminName = ? OR adminEmail = ?");
        $stmt->bind_param("ss", $admin, $admin);
        $stmt->execute();
        $stmt->store_result();
        
        // Si des informations sont trouvées, récupère les infos, sinon retourne une erreur
        if ($stmt->num_rows == 1) {            
            $stmt->bind_result($uname, $email, $passHash);
            $result = $stmt->fetch();
            
            // Si le mot de passe correspond, configure la session, sinon retourne une erreur
            if (password_verify($pass, $passHash)) {
                $_SESSION['adminName'] = $uname;
                $_SESSION['adminEmail'] = $email;
                $_SESSION['adminLoginStatus'] = 1;
                
                return true; // Retourne true en cas de succès
            } else {
                return 'Mauvais Mot de Passe'; // Retourne une erreur en cas de mot de passe incorrect
            }
            
        } else {
            return 'Nom d\'Admin ou Adresse Mail incorrect'; // Retourne une erreur si l'admin n'est pas trouvé
        }
        
    }
    
    // Vérifie si l'admin est connecté
    public function isLoggedIn() {
        
        // Retourne true si la session a été configurée, false sinon
        if(isset($_SESSION['adminLoginStatus'])) {
            return true;
        } else {
            return false;
        }
        
    }
    
    // Redirige vers une page différente
    public function redirect($url) {
        header("Location: $url");
    }
    
    // Déconnecte l'admin en détruisant la session
    public function logout() {
        session_start();
        $_SESSION = array();
        session_destroy();
    }
 
}

?>