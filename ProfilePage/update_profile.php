<?php
include 'config.php';

// Démarrage de la session et récupération de l'ID de l'utilisateur connecté
session_start();
$user_id = $_SESSION['user_id'];

// Vérification si le formulaire de mise à jour du profil a été soumis
if(isset($_POST['update_profile'])){

   // Échappement des valeurs des champs du formulaire pour éviter les injections SQL
   $update_name = mysqli_real_escape_string($conn, $_POST['update_name']);
   $update_email = mysqli_real_escape_string($conn, $_POST['update_email']);

   // Mise à jour du nom et de l'email dans la base de données
   mysqli_query($conn, "UPDATE `users` SET name = '$update_name', email = '$update_email' WHERE id = '$user_id'") or die('query failed');

   // Récupération et échappement de la nouvelle date de naissance
   $update_dob = mysqli_real_escape_string($conn, $_POST['update_dob']);

   // Mise à jour de la date de naissance dans la base de données
   mysqli_query($conn, "UPDATE `users` SET dob = '$update_dob' WHERE id = '$user_id'") or die('query failed');

   // Récupération du mot de passe actuel
   $old_pass = $_POST['old_pass'];

   // Échappement des nouveaux mots de passe
   $update_pass = mysqli_real_escape_string($conn, md5($_POST['update_pass']));
   $new_pass = mysqli_real_escape_string($conn, md5($_POST['new_pass']));
   $confirm_pass = mysqli_real_escape_string($conn, md5($_POST['confirm_pass']));

   // Vérification si les nouveaux mots de passe ne sont pas vides
   if(!empty($update_pass) || !empty($new_pass) || !empty($confirm_pass)){
      // Vérification de la correspondance de l'ancien mot de passe
      if($update_pass != $old_pass){
         $message[] = 'L\'ancien Mdp est érroné... !';
      } elseif($new_pass != $confirm_pass){
         $message[] = 'Le Mdp de Confirmation ne correspond pas... !';
      } else {
         // Mise à jour du mot de passe dans la base de données
         mysqli_query($conn, "UPDATE `users` SET password = '$confirm_pass' WHERE id = '$user_id'") or die('query failed');
         $message[] = 'Succés, le Mdp a été modifié... !';
      }
   }

   // Récupération des informations sur l'image
   $update_image = $_FILES['update_image']['name'];
   $update_image_size = $_FILES['update_image']['size'];
   $update_image_tmp_name = $_FILES['update_image']['tmp_name'];
   $update_image_folder = 'uploaded_img/'.$update_image;

   // Vérification si un fichier image a été téléchargé
   if(!empty($update_image)){
      // Vérification si la taille de l'image est inférieure à 2 mégaoctets
      if($update_image_size > 2000000){
         $message[] = 'Image is too large...';
      } else {
         // Mise à jour du chemin de l'image dans la base de données
         $image_update_query = mysqli_query($conn, "UPDATE `users` SET image = '$update_image' WHERE id = '$user_id'") or die('Query failed...!');
         if($image_update_query){
            // Déplacement de l'image téléchargée vers le dossier spécifié
            move_uploaded_file($update_image_tmp_name, $update_image_folder);
         }
         $message[] = 'Image updated successfully...!';
      }
   }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Modifications</title>
   <link rel="stylesheet" href="css/style.css">
</head>
<body>
   
<div class="update-profile">

   <?php
      $select = mysqli_query($conn, "SELECT * FROM `users` WHERE id = '$user_id'") or die('query failed');
      if(mysqli_num_rows($select) > 0){
         $fetch = mysqli_fetch_assoc($select);
      }
   ?>

   <form action="" method="post" enctype="multipart/form-data">
      <?php
         // Affichage de l'image de profil par défaut si aucune image n'est définie
         if($fetch['image'] == ''){
            echo '<img src="images/ParDéfaut.png">';
         } else {
            // Affichage de l'image de profil existante
            echo '<img src="uploaded_img/'.$fetch['image'].'">';
         }
         // Affichage des messages d'erreur ou de succès
         if(isset($message)){
            foreach($message as $message){
               echo '<div class="message">'.$message.'</div>';
            }
         }
      ?>
      <div class="flex">
         <div class="inputBox">
            <span>Nom d'Utilisateur :</span>
            <input type="text" name="update_name" value="<?php echo $fetch['name']; ?>" class="box">
            <span>Votre Adresse Mail :</span>
            <input type="email" name="update_email" value="<?php echo $fetch['email']; ?>" class="box">
            <span>Télécharger une Photo de Profil :</span>
            <input type="file" name="update_image" accept="image/jpg, image/jpeg, image/png" class="box">
            <span>Votre Date de Naissance :</span>
            <input type="date" name="update_dob" value="<?php echo $fetch['dob']; ?>" class="box">
         </div>
         <div class="inputBox">
            <p>-- Changer son mot de passe ↓ --</p>
            <input type="hidden" name="old_pass" value="<?php echo $fetch['password']; ?>">
            <span>Mot de Passe Actuel :</span>
            <input type="password" name="update_pass" placeholder="Entrez votre mot de passe actuel..." class="box">
            <span>Nouveau Mot de Passe :</span>
            <input type="password" name="new_pass" placeholder="Entrez votre nouveau mot de passe..." class="box">
            <span>Confirmer le Nouveau Mot de Passe :</span>
            <input type="password" name="confirm_pass" placeholder="Confirmez votre nouveau mot de passe..." class="box">
         </div>
      </div>
      <input type="submit" value="Enregistrer" name="update_profile" class="btn">
      <a href="home.php" class="delete-btn">Retour</a>
   </form>

</div>

</body>
</html>