<?php

Class Validate {
    
    private $db;
    
    public function __construct($conn) {
        
        $this->db = $conn;
        
    }
    
    public function adminnameValidate($uname) {

        // vide ?
        if (empty($uname)) {
            return 'Le Nom d\'Admin est vide... !'; // Retourne une erreur si le nom d'admin est vide
        } 
        // inférieure à 2 caractères ?
        elseif (strlen($uname) < 2) {
            return 'Le Nom d\'Admin est trop court... !';
        } 
        // supérieure à 64 caractères ?
        elseif (strlen($uname) > 64) {
            return 'Le Nom d\'Admin est trop long... !';
        } 
        // caractères spéciaux ?
        elseif (!preg_match('/^[a-z\d]{2,64}$/i', $uname)) {
            return 'Le Nom d\'Admin ne peut pas contenir de caractères spéciaux... !';
        } 
        // Si toutes les conditions ci-dessus sont remplies, retire les balises HTML du nom d'admin
        else {
            $uname = strip_tags($uname);
        }
            
        $stmt = $this->db->prepare("SELECT adminName FROM admins WHERE adminName = ?");
        $stmt->bind_param("s", $uname);
        $stmt->execute();
        $stmt->store_result();
        
        if ($stmt->num_rows !== 0) {
            return 'Ce Nom d\'Admin est déjà utilisé';        
        }
            
    }
        
    
    // Fonction de validation de l'email dans la classe Validate en PHP
    public function emailValidate($email) {

        // vide ?
        if(empty($email)) {
            return 'L\Adresse Mail est vide... !'; // Retourne une erreur si l'email est vide
        } 
        // format valide ?
        elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return 'L\Adresse Mail est invalide... !';
        } 
        // supérieure à 64 caractères ?
        elseif(strlen($email) > 64) {
            return 'L\Adresse Mail est trop longue... !';
        } 
        else {
            $email = strip_tags($email); // Retire les balises HTML de l'email pour des raisons de sécurité

            // Requête à la base de données pour vérifier la présence d'un email en double
            $stmt = $this->db->prepare("SELECT adminEmail FROM admins WHERE adminEmail = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $stmt->store_result();

            // Si l'email est en double, retourne une erreur, sinon retourne l'email
            if ($stmt->num_rows != 0) {
                return 'Cette Adresse Mail est déjà utilisée'; // Retourne une erreur si l'email est déjà utilisé
            } else {
                return $email; // Retourne l'email s'il passe toutes les validations
            }
        }
    }
    
    // Fonction de validation du mot de passe
    public function passwordValidate($pass, $pass2) {

        // Vérifie si le mot de passe ou la confirmation du mot de passe est vide
        if (empty($pass) || empty($pass2)) {
            return 'L\'un des Mots de Passe au moins est vide'; // Retourne une erreur si l'un des 2 mdp est vide
        } 
        // longueur du mdp inférieure à 6 caractères ?
        elseif (strlen($pass) < 6) {
            return 'Le Mot de Passe est trop court'; // Retourne une erreur si le mot de passe est trop court
        } 
        // Vérifie si le mot de passe et la confirmation du mot de passe correspondent
        elseif ($pass !== $pass2) {
            return 'Les Mots de Passe ne correspondent pas'; // Retourne une erreur si les mots de passe ne correspondent pas
        }
        // Si toutes les validations passent, retourne le mot de passe
        else {
            return $pass;
        }
    }
}

?>