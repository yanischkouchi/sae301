<?php
include 'config.php';
if(isset($_POST['submit'])){
   
   // Échappement des valeurs des champs du formulaire pour éviter les injections SQL
   $name = mysqli_real_escape_string($conn, $_POST['name']); // Nom d'utilisateur
   $email = mysqli_real_escape_string($conn, $_POST['email']); // Adresse e-mail
   $pass = mysqli_real_escape_string($conn, md5($_POST['password'])); // Mot de passe (crypté avec md5 pour la sécurité)
   $cpass = mysqli_real_escape_string($conn, md5($_POST['cpassword'])); // Confirmation du mot de passe (crypté)
   $image = $_FILES['image']['name']; // Nom du fichier image
   $image_size = $_FILES['image']['size']; // Taille de l'image
   $image_tmp_name = $_FILES['image']['tmp_name']; // Emplacement temporaire de l'image téléchargée
   $image_folder = 'uploaded_img/'.$image; // Dossier de destination pour l'image

   // Vérification si l'utilisateur avec la même adresse e-mail et mot de passe existe déjà
   $select = mysqli_query($conn, "SELECT * FROM `users` WHERE email = '$email' AND password = '$pass'") or die('query failed');
   
   if(mysqli_num_rows($select) > 0){
      $message[] = 'Cet Utilisateur existe déjà... !'; // Message d'erreur si l'utilisateur existe déjà
   }else{
      // Vérification si les mots de passe correspondent et si la taille de l'image est acceptable
      if($pass != $cpass){
         $message[] = 'Le Mdp de Confirmation ne correspond pas... !'; // Message d'erreur si les mots de passe ne correspondent pas
      }elseif($image_size > 2000000){
         $message[] = 'L\'image est trop volumineuse... !'; // Message d'erreur si la taille de l'image est trop grande
      }else{
         // Insertion des données dans la base de données si toutes les conditions sont remplies
         $insert = mysqli_query($conn, "INSERT INTO `users`(name, email, password, image) VALUES('$name', '$email', '$pass', '$image')") or die('query failed');
         if($insert){
            // Déplacement de l'image téléchargée vers le dossier spécifié
            move_uploaded_file($image_tmp_name, $image_folder);
            $message[] = 'Inscription réussie... !'; // Message de succès
            // Redirection vers la page de connexion après l'inscription réussie
            header('location:login.php');
         }else{
            $message[] = 'Inscription échouée... !'; // Message d'erreur en cas d'échec de l'inscription
         }
      }
   }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Inscription</title>
   <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="form-container">
   <form action="" method="post" enctype="multipart/form-data">

      <!-- Img cliquable redirigeant vers registeradmin.php -->
      <a href="registeradmin.php">
         <img src="images/Admin.png" alt="Logo" width="25" height="25">
      </a>

      <h3>Inscription</h3>
      <?php
      if(isset($message)){
         foreach($message as $message){
            echo '<div class="message">'.$message.'</div>';
         }
      }
      ?>
      <input type="text" name="name" placeholder="Entrez votre nom d'utilisateur..." class="box" required>
      <input type="email" name="email" placeholder="Entrez votre adresse mail..." class="box" required>
      <input type="password" name="password" placeholder="Entrez votre mot de passe..." class="box" required>
      <input type="password" name="cpassword" placeholder="Confirmez votre mot de passe..." class="box" required>
      <input type="file" name="image" class="box" accept="image/jpg, image/jpeg, image/png">
      <input type="submit" name="submit" value="S'Inscrire" class="btn">
      <p>Vous avez déjà un compte ? <a href="login.php">Se Connecter</a></p>
      <a href="../accueil.html">
         <img src="../medias/home.png" alt="Accueil" width="50" height="50" style="margin-right: 50px;">
      </a>
      <a href="../panier.html">
         <img src="../medias/panier.png" alt="Panier" width="50" height="50" style="margin-top: 50px;">
      </a>
      <a href="home.php">
         <img src="../medias/compte.png" alt="Profil" width="50" height="50" style="margin-left: 50px;">
      </a>
   </form>
</div>
</body>
</html>