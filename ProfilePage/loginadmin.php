<?php 

session_start();

include 'config.php';
// Inclusion de la classe Admin
require_once('classes/Class.Admin.php');

// Création d'une instance de la classe Admin en passant la connexion à la base de données
$admin = new Admin($conn);

// Si déjà connecté, redirection vers la page d'accueil utilisateur
if ($admin->isLoggedIn()) 
{
    $admin->redirect('home.php');
}

// Lorsque le bouton de connexion est pressé, tenter la connexion
if (isset($_POST['login'])) 
{
    $uname = $_POST['admin'];
    $pass = $_POST['password'];

    // Appel de la méthode de connexion de la classe Admin
    $login = $admin->login($uname, $pass);
    
    if ($login === true)
    {
        // Redirection vers la page d'accueil utilisateur après une connexion réussie
        $admin->redirect('home.php');
    } 
    else 
    {
        // Affichage du message d'erreur en cas d'échec de connexion
        echo $login;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Connexion</title>
        <link rel="stylesheet" href="css/style.css">
    </head>

    <body>
        <div class="form-container">

        <form method="post" action="homeadmin.php" name="loginform">

        <!-- Img cliquable redirigeant vers login.php -->
        <a href="login.php">
           <img src="images/User.png" alt="Logo" width="25" height="25">
        </a>

        <h3>Connexion</h3>
        <input type="text" name="admin" placeholder="Entrez votre nom d'admin/votre email..." class="box" required />
        <input type="password" name="password" auto_complete="off" placeholder="Entrez votre mot de passe..." class="box" required />
        <input type="submit" name="login" value="Connexion" class="btn"/>
        <p>Vous n'avez pas de compte ? <a href="registeradmin.php">S'Inscrire maintenant</a></p>
        <a href="../accueil.html">
           <img src="../medias/home.png" alt="Accueil" width="50" height="50" style="margin-right: 50px;">
        </a>
        <a href="../panier.html">
           <img src="../medias/panier.png" alt="Panier" width="50" height="50" style="margin-top: 50px;">
        </a>
        <a href="home.php">
           <img src="../medias/compte.png" alt="Profil" width="50" height="50" style="margin-left: 50px;">
        </a>
        </form>

    </body>

</html>