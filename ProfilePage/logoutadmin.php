<?php

session_start();

include 'config.php';
require_once('classes/Class.Admin.php');

// Création d'une instance de la classe Admin en passant la connexion à la base de données
$admin = new Admin($conn);

// Déconnexion de l'administrateur et redirection vers la page de connexion
if (isset($_GET['logout']))
{
    $admin->logout();
    $admin->redirect('homeadmin.php');
} 
// Si l'administrateur est toujours connecté, redirection vers homeadmin.php qui redirigera vers home.php
else
{
    $admin->redirect('homeadmin.php');
}

?>